/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

//importar vistas y modelos
import Clases.Cliente;
import Formularios.*;

import Modelos.*;
import java.sql.ResultSet;
import Formularios.Admin.*;
/**
 *
 * @author Ruben
 */
public class Controlador {

    //Vistas
    FrmLogin frmLogin;
    FrmPrincipalAdministrador frmPrincipalAdmin;
    FrmPrincipalSupervisor frmPrincipalSup;
    FrmPrincipalCallCenter frmPrincipallCC;
    FrmCrearCliente frmCrearCliente;
    FrmCrearEmpleado frmCrearEmpleado;
    
    //Modelos
    Login modLogin;
    Administrador modAdministrador;
    Supervisor modSupervisor;
    Agente modAgente;

    public Controlador() {

    }

    public void setModelos( Object modelo ) {
        switch (modelo.getClass().getName()) {

            //inicializar modelo de login desde controlador
            case "Modelos.Login":
                modLogin = (Login) modelo;
                break;
            case "Modelos.Administrador":
                this.modAdministrador = (Administrador)modelo;
                break;

            case "Modelos.Supervisor":
                this.modSupervisor = (Supervisor)modelo;
                break;     

            case "Modelos.Agente":
                this.modAgente = (Agente)modelo;
                break;                     
        }
    }

    public void setFormulario( Object formulario ) {

        switch (formulario.getClass().getName()) {

            //inicializar formulario de login
            case "Formularios.FrmLogin":
                frmLogin = (FrmLogin) formulario;
                break;

            //inicializar formulario ppal de admin  
            case "Formularios.FrmPrincipalAdministrador":
                frmPrincipalAdmin = (FrmPrincipalAdministrador)formulario;
                break;
                
            //inicializar formulario ppal de supervisor
            case "Formularios.FrmPrincipalSupervisor":
                frmPrincipalSup = (FrmPrincipalSupervisor)formulario;
                break;  
                
            //inicializar formulario ppal de call center                
            case "Formularios.FrmPrincipalCallCenter":
                frmPrincipallCC = (FrmPrincipalCallCenter)formulario;
                break; 
                
            //inicializar formulario ppal de call center                
            case "Formularios.Admin.FrmCrearCliente":
                frmCrearCliente = (FrmCrearCliente)formulario;
                break;                 
                
            //inicializar formulario ppal de call center                
            case "Formularios.Admin.FrmCrearEmpleado":
                frmCrearEmpleado = (FrmCrearEmpleado)formulario;
                break;                                    
                
            default:
                throw new AssertionError();
        }

    }

    //=========   1. METODOS PARA INICIAR APLICACION
    public void iniciarApp() {
        this.frmLogin.setVisible( true );
    }

    //=========   2. METODOS PARA FORMULARIO LOGIN
    
    //manejo de evento para click en boton ingresar
    public void clickIngresar( String user, String pass ) {
        ResultSet rs;
        if ( this.modLogin.comprobarCredenciales( user, pass ) ) {
            this.modLogin.setUsername( user );
            this.modLogin.setPassword( pass );
            try {
                rs = this.modLogin.getLogin();
                rs.next();
                int conteo = rs.getInt( "COUNT(*)" );

                //si se encontraron credenciales validas en base de datos
                if ( conteo == 1 ) {
                    rs = this.modLogin.getRol( user );
                    rs.next();
                    int rol = rs.getInt( "COD_ROL" );
                    this.frmLogin.setVisible( false);
                    this.manejoRolesUsuario( rol );
                }

                //si no se encontraron credenciales validas
                else {
                    this.frmLogin.setLabelError( "Credenciales incorrectas" );
                }
            }
            catch (Exception ex) {
                this.frmLogin.setLabelError( "Error al conectar con servidor" );
            }

        }
        else {
            this.frmLogin.setLabelError( "Campos de credenciales vacios" );
        }
    }

    private void manejoRolesUsuario( int rol ) {
        switch (rol) {

            //formulario principal de administrador
            case 1:
                this.frmPrincipalAdmin.setVisible( true );
                break;

            //formulario principal de supervisor
            case 2:
                this.frmPrincipalSup.setVisible( true );
                break;

            //formulario principal de call center
            case 3:
                this.frmPrincipallCC.setVisible( true );
                break;

            default:
                break;

        }

    }
    
    //=========   3. METODOS PARA FORMULARIO DE MENU ADMINISTRADOR
    
    public void crearCliente(){
        this.frmCrearCliente.setVisible( true);
        this.frmPrincipalAdmin.setVisible(false);
    }
    
    public void crearEmpleado(){
        this.frmCrearEmpleado.setVisible( true);
        this.frmPrincipalAdmin.setVisible(false);
    }    
    
    
    public void mostrarMenuPrincipalAdministrador(Object formulario){
        String nombreFormulario = formulario.getClass().getName();
        switch(nombreFormulario){
            case "Formulario.Admin.FrmCrearCliente":
                this.frmCrearCliente.setVisible( false);
                break;
                
            case "Formulario.Admin.FrmCrearEmpleado":
                this.frmCrearEmpleado.setVisible( false);
                break;         
        }
        this.frmPrincipalAdmin.setVisible( true );
    }
    
    //=========   4. METODOS PARA FORMULARIO DE MENU SUPERVISOR
    
    //=========   5. METODOS PARA FORMULARIO DE MENU CALL CENTER
    
    //=========   6. METODOS PARA FORMULARIOS DE CREACION DE USUARIOS
    
    public void btnCrearCliente(){
        Cliente cl = this.modAdministrador.getCliente();

        if(cl.validarInformacion( this.frmCrearCliente.txtPrimerNombre.getText(), 
                this.frmCrearCliente.txtPrimerApellido.getText(), 
                this.frmCrearCliente.txtFecNamiento.getText(), 
                this.frmCrearCliente.cbbGenero.getItemAt( this.frmCrearCliente.cbbGenero.getSelectedIndex() ),
                this.frmCrearCliente.txtTelefono.getText()
        ))
        {
            Cliente clienteIngresado = new Cliente(
                    this.frmCrearCliente.txtPrimerNombre.getText(),
                    this.frmCrearCliente.txtSegundoNombre.getText(),
                    this.frmCrearCliente.txtPrimerApellido.getText(),
                    this.frmCrearCliente.txtSegundoApellido.getText(), 
                    this.frmCrearCliente.txtFecNamiento.getText(),
                    this.frmCrearCliente.cbbGenero.getItemAt( this.frmCrearCliente.cbbGenero.getSelectedIndex()),
                    Long.parseLong( this.frmCrearCliente.txtDui.getText()),// dui
                    Long.parseLong( this.frmCrearCliente.txtTelefono.getText()),
                    this.frmCrearCliente.txtDireccion.getText()
            );
            
            this.modAdministrador.crearCliente();
        }
        
        //si los campos estan vacios
        else{
            this.frmCrearCliente.lblManejoErrores( "Campos vacios" );
        }
        //si algun campo esta vacio        
        

    }
    
    
    
}
