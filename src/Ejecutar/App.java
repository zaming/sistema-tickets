/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejecutar;

//importar vistas, controladores y modelos
import Controladores.*;
import Formularios.*;
import Formularios.Admin.*;
import Modelos.*;

/**
 *
 * @author Ruben
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main( String[] args ) {

        //formularios
        FrmLogin frmLogin = new FrmLogin();
        FrmPrincipalAdministrador frmPrincipalAdmin = new FrmPrincipalAdministrador();
        FrmPrincipalSupervisor frmPrincipalSup = new FrmPrincipalSupervisor();
        FrmPrincipalCallCenter frmPrincipallCC = new FrmPrincipalCallCenter();
        FrmCrearCliente frmCrearCliente = new FrmCrearCliente();
        FrmCrearEmpleado frmCrearEmpleado = new FrmCrearEmpleado();

        //modelos
        Login modLogin = new Login();
        Administrador modAdmin = new Administrador();
        Agente modAgente = new Agente();
        Supervisor modSupervisor = new Supervisor();

        //controladores
        Controlador controlador = new Controlador();
        
        //asignar modelos a controlador
        controlador.setModelos( modLogin );
        controlador.setModelos( modAdmin );
        controlador.setModelos( modAgente );
        controlador.setModelos( modSupervisor );

        
        //asignar formularios a controlador
        controlador.setFormulario( frmLogin );
        controlador.setFormulario( frmPrincipalAdmin);
        controlador.setFormulario( frmPrincipalSup);
        controlador.setFormulario( frmPrincipallCC);
        controlador.setFormulario( frmCrearCliente);
        controlador.setModelos( frmCrearEmpleado );
        
        //asignar controlador a formularios
        frmLogin.setControlador( controlador );
        frmPrincipalAdmin.setControlador( controlador );
        frmPrincipalSup.setControlador( controlador );
        frmPrincipallCC.setControlador( controlador );
        frmCrearCliente.setControlador( controlador );
        frmCrearEmpleado.setControlador( controlador );
        
        
        //iniciar aplicacion
        controlador.iniciarApp();
    }

}
