/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.FileInputStream;
import java.util.Properties;

/**
 *
 * @author Ruben
 */
public class Propiedades {
    private String directorio;
    
    public Propiedades(){
        this.directorio = System.getProperty( "user.dir");
    }
    
    private String getAppPropsFile(){
        String archivo = System.getProperty("file.separator") + "app.properties";
        return this.directorio + archivo ;
    }
    
    private String getDBPropsFile(){
        String archivo = System.getProperty("file.separator") + "db.properties";
        return this.directorio + archivo ;
    }
    
    public Properties getAppProps(){
        Properties props = new Properties();
        try{
            props.load( new FileInputStream(this.getAppPropsFile()));    
        }
        catch(Exception ex){
            
        }
        
        return props;
    }
    
    public Properties getDBProps(){
        Properties props = new Properties();
        try{
            props.load( new FileInputStream(this.getDBPropsFile()));
        }
        catch(Exception ex){
            
        }
        return props;
    }
    
}
