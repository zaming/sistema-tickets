/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author Ruben
 */
public class Cliente extends Persona {
    private int idCliente;
    
    public Cliente(){
        
    }
    
    public Cliente(String primerNom, String primerApe){
        super(primerNom, primerApe);
    }
    
    public Cliente(String primNom, String segNom, String primApe, String segApe,
                   String fecNac, String genPersona, Long dui, Long tel, String dir){
        super(primNom, segNom, primApe, segApe, fecNac, genPersona, dui, tel, dir );

    }    
    
    public void setIdCliente(int id){
        this.idCliente = id;
    }
    
    public int getIdCliente(){
        return this.idCliente;
    }
    
    public Boolean validarInformacion(String primNombre, String primApe, String fecNac, String gen, String tel){
        Boolean camposValidos = false;
        
        //si existe un campo principal vacio
        if(primNombre.equals( "") || primApe.equals( "") || fecNac.equals( "") || gen.equals( "") || tel.equals( "")){
            camposValidos = false;
        }
        else{
            try{
                
                //si la longitud del telefono es la adecuada
                if(tel.length() == 8){
                    Long telefono = Long.parseLong( tel );
                    camposValidos = true;
                }
                
                //si longitud de telefono no es valida
                else{
                    camposValidos = false;
                }
            }
            catch (Exception ex){
                camposValidos = false;
            }
        }
        return camposValidos;
    }
}
