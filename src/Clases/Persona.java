/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author Ruben
 */
public class Persona {
    protected int idPersona;
    protected String primNombre;
    protected String segNombre;
    protected String primApellido;
    protected String segApellido;
    protected String fecNacimiento;
    protected String genPersona;
    protected Long duiPersona;
    protected long telefonoPersona;
    protected String direccionPersona;
    
    public Persona(){
        
    }
    
    public Persona(String primerNom, String primerApe){
        this.primNombre = primerNom;
        this.primApellido = primerApe;
    }
    
    public Persona(String primNom, String segNom, String primApe, String segApe,
                   String fecNac, String genPersona, Long dui, Long tel, String dir){
        this.primNombre = primNom;
        this.segNombre = segNom;
        this.primApellido = primApe;
        this.segApellido = segApe;
        this.fecNacimiento = fecNac;
        this.genPersona = genPersona;
        this.duiPersona = dui;
        this.telefonoPersona = tel;
        this.direccionPersona = dir;
    }
    
    //===========   SETTERS  =========//
    
    public  void setIdPersona(int id){
        this.idPersona = id;
    }
    
    public void setPrimNombre(String nombre){
        this.primNombre = nombre;
    }
    
    public void setSegNombre(String nombre){
        this.segNombre = nombre;
    }    
    
    public void setPrimApellido(String nombre){
        this.primApellido = nombre;
    }    
    
    public void setSegApellido(String nombre){
        this.segApellido = nombre;
    }    
    
    public void setFecNacimiento(String fecha){
        this.fecNacimiento = fecha;
    }    
    
    public void setGenPersona(String genPersona){
        this.genPersona = genPersona;
    }    
    
    public void setDuiPersona(Long dui){
        this.duiPersona = dui;
    }    
    
    public void setTelefonoPersona(Long telefono){
        this.telefonoPersona = telefono;
    }    
    
    public void setDireccion(String dir){
        this.direccionPersona = dir;
    }
    
    //===========   GETTERS  =========//
    
    public String getPrimNombre(){
        return this.primNombre;
    }            
            
    public int getIdPersona(){
        return this.idPersona;
    }
    
    public String getSegNombre(){
        return this.segNombre;
    }    
    
    public String getPrimApellido(){
        return this.primApellido;
    }   
    
    public String getSegApellido(){
        return this.segApellido;
    }    
    
    public String getFechaNacimiento(){
        return this.fecNacimiento;
    }    
    
    public String getGenPersona(){
        return this.genPersona;
    }  

    public Long getDuiPersona(){
        return this.duiPersona;
    }  
    
    public Long getTelefonoPersona(){
        return this.telefonoPersona;
    }    
    
    public String getDireccion(){
        return this.direccionPersona;
    }
    
}
